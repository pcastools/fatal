// Fatal provides a literate (and easily searchable for) way of informing the user that an unrecoverable and unexpected situation has occurred. Typically this message will be wrapped in a panic.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package fatal

import (
	"bytes"
	"fmt"
	"strings"
	"unicode"
)

// Standard messages.
const (
	notImplemented      = "Not yet implemented"
	impossibleToGetHere = "Impossible to get here"
	neverCalled         = "This should never be called"
	impossibleError     = "An error which should be impossible has occurred"
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// isWhiteSpace returns true iff the given string consists entirely of white space, as determined by the unicode function IsSpace.
func isWhiteSpace(s string) bool {
	return strings.IndexFunc(s, func(r rune) bool {
		return !unicode.IsSpace(r)
	}) == -1
}

// createString returns the correctly formatted string.
func createString(base string, format string, args ...interface{}) string {
	if len(format) != 0 {
		if msg := fmt.Sprintf(format, args...); !isWhiteSpace(msg) {
			return base + ": " + msg
		}
	}
	return base + "."
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// NotImplemented returns a string stating that the current code path is not implemented.
func NotImplemented() string {
	return NotImplementedf("")
}

// NotImplementedf returns a string stating that the current code path is not implemented. Additional information, formated according to the given format specifier, will be appended to the string.
func NotImplementedf(format string, args ...interface{}) string {
	return createString(notImplemented, format, args...)
}

// ImpossibleToGetHere returns a string stating the the current code path is (supposedly) impossible to reach.
func ImpossibleToGetHere() string {
	return ImpossibleToGetHeref("")
}

// ImpossibleToGetHeref returns a string stating the the current code path is (supposedly) impossible to reach. Additional information, formated according to the given format specifier, will be appended to the string.
func ImpossibleToGetHeref(format string, args ...interface{}) string {
	return createString(impossibleToGetHere, format, args...)
}

// NeverCalled returns a string stating the the current code path should never be called (for example, the method only exists to satisfy an interface).
func NeverCalled() string {
	return NeverCalledf("")
}

// NeverCalledf returns a string stating the the current code path should never be called (for example, the method only exists to satisfy an interface). Additional information, formated according to the given format specifier, will be appended to the string.
func NeverCalledf(format string, args ...interface{}) string {
	return createString(neverCalled, format, args...)
}

// ImpossibleError returns a string describing the (supposedly) impossible error.
func ImpossibleError(err error) string {
	return ImpossibleErrorf(err, "")
}

// ImpossibleErrorf returns a string describing the (supposedly) impossible error. Additional information, formated according to the given format specifier, will be appended to the string.
func ImpossibleErrorf(err error, format string, args ...interface{}) string {
	msg := createString(impossibleError, format, args...)
	// Append the error description to the message
	b := new(bytes.Buffer)
	fmt.Fprintf(b, "%+v", err)
	if errdesc := b.String(); !isWhiteSpace(errdesc) {
		return msg + "\n" + errdesc
	}
	return msg
}
